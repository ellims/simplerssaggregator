﻿using System;
using System.Windows;
using System.Windows.Controls;
using SImpleRSSAggregator.RssAggregatorService;
using SImpleRSSAggregator.ChildWindows;

namespace SImpleRSSAggregator
{
    public partial class MainPage : UserControl
    {
        public MainPage()
        {
            InitializeComponent();
            SetUp();
        }

        #region client GUI methods

        /// <summary>
        /// Sets start options
        /// </summary>
        private void SetUp()
        {

            RSSnewsDataGrid.Visibility = System.Windows.Visibility.Collapsed;
            RssDataGrid.Visibility = System.Windows.Visibility.Visible;
            ViewFeedButton.Visibility = System.Windows.Visibility.Collapsed;
            UpdateForm();
        }

        /// <summary>
        /// Shows attention window with  exception details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ex"></param>
        private void ShowMessage(string sender, Exception ex)
        {
            MessageWindow exceptionWindow = new MessageWindow();
            exceptionWindow.Title = "Attention! Exception in " + sender;
            exceptionWindow.MessageTextBlock.Text = ex.Message;

            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
                exceptionWindow.MessageTextBlock.Text += "\r\n" + ex.Message + "\r\n" + ex.StackTrace;
            }
            exceptionWindow.Show();
        }        

        /// <summary>
        /// Updates GUI 
        /// </summary>
        public void UpdateForm()
        {

            RssAggregatorServiceClient client = new RssAggregatorServiceClient();
            client.OpenAsync();
            try
            {
                if (RssDataGrid.Visibility == System.Windows.Visibility.Visible)
                {
                    client.GetFeedsCompleted += client_GetFeedsCompleted;
                    client.GetFeedsAsync();

                    ViewFeedButton.Visibility = System.Windows.Visibility.Collapsed;
                }

                else if (RssDataGrid.SelectedItem != null && RSSnewsDataGrid.Visibility == System.Windows.Visibility.Visible)
                {
                    client.GetFeedNewsCompleted += client_GetNewsCompleted;
                    client.GetFeedNewsAsync(((RSSFeed)RssDataGrid.SelectedItem).ID);
                    ViewFeedButton.Visibility = System.Windows.Visibility.Visible;
                }

                else if (RssDataGrid.SelectedItem == null && RSSnewsDataGrid.Visibility == System.Windows.Visibility.Visible)
                {
                    client.FetchAllFeedNewsCompleted += client_FetchAllFeedNewsCompleted;
                    client.FetchAllFeedNewsAsync();

                    ViewFeedButton.Visibility = System.Windows.Visibility.Visible;
                }
            }

            catch (Exception ex)
            {
                ShowMessage("UpdateForm", ex);
            }
            finally
            {
                client.CloseAsync();
            }
        }

        #endregion

        #region Client Event Handlers

        /// <summary>
        /// Only calls method UpdateForm()
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateForm_EventHandler(object sender, EventArgs e)
        {
            UpdateForm();
        }

        /// <summary>
        /// Calls the AddWindow dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddFeedButton_Click(object sender, RoutedEventArgs e)
        {
            var addFeedWindow = new AddWindow(this);
            addFeedWindow.Show();
        }

        /// <summary>
        /// Deletes a feed or news via pushing the "Delete" button on the grid-row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Delete_SingleItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RssDataGrid.Visibility == System.Windows.Visibility.Visible)
                {
                    RssAggregatorServiceClient client = new RssAggregatorServiceClient();

                    // client.OpenAsync();
                    client.RemoveFeedCompleted += UpdateForm_EventHandler;
                    client.RemoveFeedAsync(((RSSFeed)RssDataGrid.SelectedItem).ID);
                    client.CloseAsync();
                }
                else
                {
                    RssAggregatorServiceClient client = new RssAggregatorServiceClient();

                    //client.OpenAsync();
                    client.RemoveNewsCompleted += UpdateForm_EventHandler;
                    client.RemoveNewsAsync(((RSSNewsItem)RSSnewsDataGrid.SelectedItem).ID);
                    client.CloseAsync();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Delete_SingleItem_Click", ex);
            }
        }

        /// <summary>
        /// Calls the Delete Warning Dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Delete_All_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var warningMessage = new MessageWindow();
                warningMessage.Title = "Warning!";
                warningMessage.MessageTextBlock.Text = "Warning! This action will delete\r\n all saved feeds from database.\r\n Are you sure?";
                warningMessage.Show();
            }
            catch (Exception ex)
            {
                ShowMessage("Delete_All_Click", ex);
            }
        }

        /// <summary>
        /// Back to feed view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewFeed_Click(object sender, RoutedEventArgs e)
        {
            RssDataGrid.Visibility = System.Windows.Visibility.Visible;
            RSSnewsDataGrid.Visibility = System.Windows.Visibility.Collapsed;
            UpdateForm();
        }

        /// <summary>
        /// Fetchs the news from all feeds
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FetchAllFeed_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RssAggregatorServiceClient client = new RssAggregatorServiceClient();
                client.OpenAsync();
                client.FetchAllFeedNewsCompleted += client_FetchAllFeedNewsCompleted;
                client.FetchAllFeedNewsAsync();
                client.CloseAsync();

                RssDataGrid.Visibility = System.Windows.Visibility.Collapsed;
                RSSnewsDataGrid.Visibility = System.Windows.Visibility.Visible;
                ViewFeedButton.Visibility = System.Windows.Visibility.Visible;
            }
            catch (Exception ex)
            {
                ShowMessage("FetchAllFeed_Click", ex);
            }
        }

        /// <summary>
        /// Sets the column`s order in RssDataGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RssDataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            try
            {
                switch ((string)e.Column.Header)
                {
                    case "ID": e.Column.Visibility = System.Windows.Visibility.Collapsed; break;        //hide unusefull columns
                    case "URL": e.Column.Visibility = System.Windows.Visibility.Collapsed; break;       //
                    case "news": e.Column.Visibility = System.Windows.Visibility.Collapsed;             //

                            RssDataGrid.Columns[0].DisplayIndex = 4; break;                                     //
                    case "Name": e.Column.DisplayIndex = 0; break;                                          //
                    case "Title": e.Column.DisplayIndex = 1; e.Column.MaxWidth = 200; break;                 //Change the column`s order in datagrid
                    case "Description": e.Column.DisplayIndex = 2; e.Column.MaxWidth = 600; break;         //
                }
            }
            catch (Exception ex)
            {
                ShowMessage("RssDataGrid_AutoGeneratingColumn", ex);
            }

        }

        /// <summary>
        /// Sets the column`s order in RSSnewsDataGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RSSnewsDataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            try
            {
                switch ((string)e.Column.Header)
                {
                    case "Channel_ID": e.Column.Visibility = System.Windows.Visibility.Collapsed; break;
                    case "ID": e.Column.Visibility = System.Windows.Visibility.Collapsed; break;
                    case "Title": e.Column.DisplayIndex = 0; RSSnewsDataGrid.Columns[0].DisplayIndex = 3; break;         //
                    case "Description": e.Column.DisplayIndex = 1; e.Column.MaxWidth = 800; break;                     //Set the column`s order in datagrid
                    case "URL": e.Column.DisplayIndex = 2; break;                                                  // 
                }
            }
            catch (Exception ex)
            {
                ShowMessage("RSSnewsDataGrid_AutoGeneratingColumn", ex);
            }
        }

        /// <summary>
        /// Fills RssDataGrid with all feeds
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RssDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (e.AddedItems.Count != 0 && ((DataGrid)sender).CurrentColumn.Header != null)
                {
                    RssAggregatorServiceClient client = new RssAggregatorServiceClient();
                    //   client.OpenAsync();
                    client.GetFeedNewsCompleted += client_GetNewsCompleted;
                    client.GetFeedNewsAsync(((RSSFeed)e.AddedItems[0]).ID);
                    client.CloseAsync();

                    RssDataGrid.Visibility = System.Windows.Visibility.Collapsed;
                    RSSnewsDataGrid.Visibility = System.Windows.Visibility.Visible;
                    UpdateForm();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("RssDataGrid_SelectionChanged", ex);
            }
        }

        /// <summary>
        /// Provides a navigation to URL by click on Link column in RSSnewsDataGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RSSnewsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (((DataGrid)sender).CurrentColumn.Header != null && ((DataGrid)sender).CurrentColumn.Header.ToString() == "URL")
                {
                    System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(((RSSNewsItem)((DataGrid)sender).SelectedItem).URL));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("RSSnewsDataGrid_SelectionChanged", ex);
            }
        }

#endregion

        #region Service Event Handlers

        /// <summary>
        /// Sets the source for RssDataGrid.
        /// e.Result is RssChannel collection type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void client_GetFeedsCompleted(object sender, GetFeedsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ShowMessage("client_GetFeedsCompleted", e.Error);
            }
            else
            {
                try
                {
                    RssDataGrid.ItemsSource = e.Result;
                }
                catch (Exception ex)
                {
                    ShowMessage("client_GetFeedsCompleted", ex);
                }
            }
        }

        /// <summary>
        /// Sets the source for RSSnewsDataGrid.
        /// e.Result is RssNews collection type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void client_GetNewsCompleted(object sender, GetFeedNewsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ShowMessage("client_GetNewsCompleted", e.Error);
            }
            else
            {
                try
                {
                    RSSnewsDataGrid.ItemsSource = e.Result;
                }
                catch (Exception ex)
                {
                    ShowMessage("client_GetNewsCompleted", ex);
                }
            }
        }

        /// <summary>
        /// Fills RSSnewsDataGrid with all news from all feeds
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void client_FetchAllFeedNewsCompleted(object sender, FetchAllFeedNewsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ShowMessage("client_FetchAllFeedNewsCompleted", e.Error);
            }
            else
            {
                try
                {
                    RSSnewsDataGrid.ItemsSource = e.Result;
                }
                catch (Exception ex)
                {
                    ShowMessage("client_FetchFeedNewsCompleted", ex);
                }
            }
        }

        #endregion
    }
}
