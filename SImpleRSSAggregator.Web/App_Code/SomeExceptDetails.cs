﻿using System;
using System.Runtime.Serialization;

namespace SImpleRSSAggregator.Web
{
    [DataContract]
    class SomeExceptDetails
    {
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string StackTrace { get; set; }

        public SomeExceptDetails() { }
        public SomeExceptDetails(Exception ex)
        {
            this.Message = ex.Message;
            this.StackTrace = ex.StackTrace;
        }
    }
}
