﻿using System.Data.SqlClient;
using System.Runtime.Serialization;

namespace SImpleRSSAggregator.Web
{
    [DataContract]
    public class SqlExceptDetails
    {
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string StackTrace { get; set; }

        public SqlExceptDetails() { }
        public SqlExceptDetails(SqlException ex)
        {
            this.Message = ex.Message;
            this.StackTrace = ex.StackTrace;
        }
    }
}
