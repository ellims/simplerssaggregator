﻿using System.Runtime.Serialization;
using System.Xml;

namespace SImpleRSSAggregator.Web
{
    [DataContract]
    public class XmlExceptDetails
    {
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string StackTrace { get; set; }

        public XmlExceptDetails() { }
        public XmlExceptDetails(XmlException ex)
        {
            this.Message = ex.Message;
            this.StackTrace = ex.StackTrace;
        }
    }
}
