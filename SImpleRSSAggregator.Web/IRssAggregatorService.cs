﻿using SImpleRSSAggregator.Web.Model;
using System.Collections.Generic;
using System.ServiceModel;

namespace SImpleRSSAggregator.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRssAggregatorService" in both code and config file together.
    [ServiceContract]
    //[ServiceKnownType(typeof(Channel))]
   // [ServiceKnownType(typeof(News))]
    public interface IRssAggregatorService
    {
        [OperationContract]
        [FaultContract(typeof(SqlExceptDetails))]
        [FaultContract(typeof(XmlExceptDetails))]
        [FaultContract(typeof(SomeExceptDetails))]
        string AddFeed(string url, string name);

        [OperationContract]
        [FaultContract(typeof(SqlExceptDetails))]
        [FaultContract(typeof(SomeExceptDetails))]
        void RemoveFeed(int feed_Id);

        [OperationContract]
        [FaultContract(typeof(SqlExceptDetails))]
        [FaultContract(typeof(SomeExceptDetails))]
        List<RSSFeed> GetFeeds();

        [OperationContract]
        [FaultContract(typeof(SqlExceptDetails))]
        [FaultContract(typeof(SomeExceptDetails))]
        RSSNews FetchAllFeedNews();

        [OperationContract]
        [FaultContract(typeof(SqlExceptDetails))]
        [FaultContract(typeof(SomeExceptDetails))]
        RSSNews GetFeedNews(int channel_id);

        [OperationContract]
        [FaultContract(typeof(SqlExceptDetails))]
        [FaultContract(typeof(SomeExceptDetails))]
        void RemoveNews(int item_id);

        [OperationContract]
        [FaultContract(typeof(SqlExceptDetails))]
        [FaultContract(typeof(SomeExceptDetails))]
        void RemoveAllNews();
    }
}
